import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * Auto-generated code below aims at helping you parse the standard input according to the problem statement.
 **/
class Player {

	public void nonStaticMain() {
		//Current Time
		Scanner in = new Scanner(System.in);
		int R = in.nextInt(); // number of rows.
		int C = in.nextInt(); // number of columns.
		int A = in.nextInt(); // number of rounds between the time the alarm countdown is activated and the time the alarm goes off.

		Point myPosition;
		Point startPoint = null;
		Map map = new Map(R, C);

		boolean controlFound = false;
		List<Point> path = new ArrayList<>();

		// game loop
		int loop = 0;
		while (true) {
			String nextPos = "RIGHT";
			int KR = in.nextInt(); // row where Kirk is located.
			int KC = in.nextInt(); // column where Kirk is located.
			myPosition = new Point(KC, KR);

			if (startPoint == null) {
				startPoint = myPosition;
			}

			map.updateMap(in);
			map.printMap();
			if (!controlFound) {
				controlFound = map.getTile(myPosition) == map.COMMAND;
				if (controlFound) {
					path = pathfinding(myPosition, startPoint, map, false);
					System.err.println("controlFound:[" + myPosition + "] pathBackStartPoint:" + path);
					path.remove(0);
				}
			}
			if (controlFound) {
				// System.out.println("UP");

				Point nextUk = path.remove(0);
				nextPos = getDir(myPosition, nextUk);
			}
			else {
				if (path.isEmpty()) {
					Point newuk = map.nextUnknowPos(myPosition);
					path = pathfinding(myPosition, newuk, map, true);
					path.remove(0);

					System.err.println("myPosition:[" + myPosition + "] + nextUk:[" + newuk + "]         " + path);

				}

				Point nextUk = path.remove(0);
				if (map.isValide(true, nextUk)) {
					nextPos = getDir(myPosition, nextUk);
				}
				else {
					System.err.println("next is invalide myPosition:[" + myPosition + "] + nextUk:[" + nextUk + "]        ");
					Point newuk = map.nextUnknowPos(myPosition);
					path = pathfinding(myPosition, newuk, map, true);
					path.remove(0);
					System.err.println("next is invalide myPosition:[" + myPosition + "] + nextUk:[" + newuk + "]        " + path);
					nextUk = path.remove(0);
					nextPos = getDir(myPosition, nextUk);
				}

				System.err.println("myPosition:[" + myPosition + "] + nextUk:[" + nextUk + "] nextPos [" + nextPos + "]");
			}

			// Write an action using System.out.println()
			// To debug: System.err.println("Debug messages...");

			// if (loop > 10) {
			// nextPos = "UP";
			// }

			System.out.println(nextPos); // Kirk's next move (UP DOWN LEFT or RIGHT).
			loop++;
		}
	}

	public static void main(String args[]) {
		Player p = new Player();

		p.nonStaticMain();
	}

	private class PathfindingPoint {
		public PathfindingPoint	cameFrom;
		public Point			point;
		public double			gScore;
		public double			fScore;

		public PathfindingPoint(Point point) {
			super();
			this.point = point;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((point == null) ? 0 : point.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) return true;
			if (obj == null) return false;
			if (getClass() != obj.getClass()) return false;
			PathfindingPoint other = (PathfindingPoint) obj;
			if (!getOuterType().equals(other.getOuterType())) return false;
			if (point == null) {
				if (other.point != null) return false;
			}
			else if (!point.equals(other.point)) return false;
			return true;
		}

		private Player getOuterType() {
			return Player.this;
		}

	}

	private double heuristic(Point startPoint, Point goalPoint) {
		return startPoint.distance(goalPoint);
		// return Vector2.dst(start.x, start.y, goal.x, goal.y);
	}

	private List<Point> reconstructPath(PathfindingPoint current) {
		List<Point> totalPath = new ArrayList<Point>();
		totalPath.add(current.point);
		while (current.cameFrom != null) {
			current = current.cameFrom;
			totalPath.add(current.point);
		}
		Collections.reverse(totalPath);
		return totalPath;
	}

	public List<Point> pathfinding(Point startPoint, Point goalPoint, Map map, boolean withUnknown) {
		List<PathfindingPoint> closedset = new ArrayList<PathfindingPoint>();
		List<PathfindingPoint> openset = new ArrayList<PathfindingPoint>();

		PathfindingPoint start = new PathfindingPoint(startPoint);
		openset.add(start);
		start.gScore = 0;
		start.fScore = start.gScore + heuristic(startPoint, goalPoint);

		while (!openset.isEmpty()) {
			// the node in openset having the lowest f_score[] value
			PathfindingPoint current = openset.get(0);

			if (current.point.equals(goalPoint)) {
				List<Point> path = reconstructPath(current);
				return path;
			}

			openset.remove(current);
			closedset.add(current);

			for (Point n : map.nextPointValide(current.point, withUnknown)) {

				PathfindingPoint neighbor = new PathfindingPoint(n);
				if (!closedset.contains(neighbor)) {
					double gScoreTentative = current.gScore + 1;

					if (!openset.contains(neighbor) || gScoreTentative < neighbor.gScore) {
						neighbor.cameFrom = current;
						neighbor.gScore = gScoreTentative;
						neighbor.fScore = neighbor.gScore + heuristic(neighbor.point, goalPoint);

						if (!openset.contains(neighbor)) {
							openset.add(neighbor);
						}
					}
				}
			}

			Collections.sort(openset, new Comparator<PathfindingPoint>() {
				@Override
				public int compare(PathfindingPoint arg0, PathfindingPoint arg1) {
					return Double.compare(arg0.fScore, arg1.fScore);
				}
			});
		}

		return null;
	}

	private String getDir(Point me, Point nextP) {
		if (nextP.x > me.x) {
			return "RIGHT";
		}
		if (nextP.x < me.x) {
			return "LEFT";
		}
		if (nextP.y > me.y) {
			return "DOWN";
		}
		if (nextP.y < me.y) {
			return "UP";
		}
		return "RIGHT";
	}

	private class Map {

		public char	WALL			= '#';
		public char	UNKNOWN			= '?';
		public char	EMPTY			= '.';
		public char	START			= 'T';
		public char	COMMAND			= 'C';
		public char	OUT_OF_BOUND	= '$';

		int			width;
		int			height;
		char[][]	map;

		public Map(int height, int width) {
			this.width = width;
			this.height = height;
			map = new char[height][width];

			System.err.println("map " + width + "x" + height + " init");
		}

		public List<Point> nextPointValide(Point point, boolean withUnknown) {
			List<Point> ps = new ArrayList<>();
			for (int i = 0; i < 4; i++) {
				Point p = nextPointindex(point, i);
				if (isValide(withUnknown, p)) {
					ps.add(p);
				}
			}

			return ps;
		}

		private boolean isValide(boolean withUnknown, Point p) {
			return getTile(p) == EMPTY || getTile(p) == START || getTile(p) == COMMAND || (withUnknown && getTile(p) == UNKNOWN);
		}

		public void updateMap(Scanner in) {
			for (int y = 0; y < height; y++) {
				String ROW = in.next(); // C of the characters in '#.TC?' (i.e. one line of the ASCII maze).
				for (int x = 0; x < ROW.length(); x++) {
					map[y][x] = ROW.charAt(x);
				}

			}
		}

		public void printMap() {
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					System.err.print("" + map[y][x]);
				}
				System.err.println("");
			}

		}

		private char getTile(Point p) {
			if (!inBound(p)) {
				return OUT_OF_BOUND;
			}
			return map[p.y][p.x];
		}

		private boolean inBound(Point p) {
			return 0 <= p.x && p.x < width && 0 <= p.y && p.y < height;
		}

		public Point nextUnknowPos(Point p) {
			Point nextPoint = null;
			List<Point> openPoint = new ArrayList<>();
			List<Point> closePoint = new ArrayList<>();
			openPoint.add(p);
			while (nextPoint == null && !openPoint.isEmpty()) {
				p = openPoint.remove(0);
				System.err.println("nextUnknowPos test : " + p);
				closePoint.add(p);
				if (getTile(p) == UNKNOWN || getTile(p) == COMMAND) {
					nextPoint = p;
				}
				else {
					for (int i = 0; i < 4; i++) {
						Point ppp = nextPointindex(p, i);

						if (!closePoint.stream().anyMatch(www -> www.equals(ppp)) && getTile(ppp) != OUT_OF_BOUND && getTile(ppp) != WALL) {
							System.err.println("nextUnknowPos ppp : " + ppp);
							openPoint.add(ppp);
						}
					}
				}
			}

			return nextPoint;
		}

		private Point nextPointindex(Point p, int i) {
			Point nextPoint;
			switch (i) {
				case 0:
					nextPoint = new Point(p.x + 1, p.y);
				break;
				case 1:
					nextPoint = new Point(p.x - 1, p.y);
				break;
				case 2:
					nextPoint = new Point(p.x, p.y + 1);
				break;
				case 3:
					nextPoint = new Point(p.x, p.y - 1);
				break;

				default:
					nextPoint = new Point(p.x + 1, p.y);
				break;
			}
			return nextPoint;
		}
	}
}